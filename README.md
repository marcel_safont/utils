# Utils for my daily life

Custom scripts to make my life easier

## Installation

1. To call scripts anywhere you have to make sure you haver python in your path in .bash_profile or .zshrc
2. In order to use performance.py you must have installed
    1. node.js
    2. puppetter (https://github.com/puppeteer/puppeteer)
    3. lighthouse-batch (https://www.npmjs.com/package/lighthouse-batch)
    4. beautifulsoup4 (https://pypi.org/project/beautifulsoup4/)

