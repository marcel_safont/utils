#!/usr/local/bin/python3

import os
from lxml import etree
import requests
import json
import re
from bs4 import BeautifulSoup

def main():
	url = checkUrl()
	urls_to_analyze = getUrlsToAnalyze(url)
	score = getScore()
	launchAnalysis(urls_to_analyze)
	getResults(score)

def checkUrl():
	try:
		while True:
			r = input('Enter a valid sitemap url:\n')
			if(r):
				return r
	except Exception as e:
		print(e)

def getUrlsToAnalyze(url):
	limit = input('Choose limit of urls to analyze (empty means everything):\n')
	if(limit == ''):
		limit = 'None'
	else:
		limit = int(limit)
	r = requests.get(url)
	soup = BeautifulSoup(r.content, 'html.parser')
	elements = soup.findAll('loc')
	return [element.text for element in elements[:limit]]

def launchAnalysis(urls):
	my_command = "lighthouse-batch -s {0}".format(",".join(map(str, urls)))
	print(my_command)
	os.system(my_command)

def getResults(score):
	bad_performance = []
	with open('./report/lighthouse/summary.json', 'r') as f:
		array = json.load(f)
	count = 0
	for dic in array:
		if float(dic['score']) < float(score):
			bad_performance.append(dic)
			count += 1
	print('Bad results:\n')
	if(len(bad_performance) == 0):
		print('No results')
	else:
		for url in bad_performance:
			print('La URL {0} ha obtingut {1} score'.format(url['url'], url['score']))

def getScore():
	while True:
		score = input('Choose score minimum, must be between 0 and 99:\n')
		if(int(score) in range(0, 99)):
			return int(score)/100

if __name__ == "__main__":
	main()